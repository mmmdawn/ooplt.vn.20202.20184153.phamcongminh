# WEEK 1

***
This week's goal is to become familiar with Java and it's really fun. It reminds me of the 20182 semester when I was a first-year student and learning to code C.  
Java keywords and syntaxes are quite similar to C, that's why it's so easy to make a start with Java.

Let's have a look at the table below:

| Filename | Exercise | Note |
| --- | --- | --- |
| HelloWorld.java | exercise 1 | |
| FirstDialog.java | exercise 2 | |
| TestDialog.java | exercise 3 | |
| TwoNumbers.java | exercise 4 | |
| ex5.java | exercise 5 | |
| FirstDegreeEquation.java | exercise 6 | Homework |
| SystemOfFirstDegreeEquations.java | exercise 7 | Homework |
| SecondDegreeEquation.java | exercise 8 | Homework |
***
