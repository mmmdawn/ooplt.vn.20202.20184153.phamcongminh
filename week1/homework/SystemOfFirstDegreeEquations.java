package homework;
import javax.swing.JOptionPane;
public class SystemOfFirstDegreeEquations {
    public static void main(String[] args){
        JOptionPane.showMessageDialog(null, "Bạn đang ở chương trình giải hệ phương trình bậc nhất 2 ẩn\nax + b = c\ndy + e = f\n", "Hello", JOptionPane.INFORMATION_MESSAGE);
        String strA = JOptionPane.showInputDialog(null, "Vui lòng nhập a: ", "ax + b = c", JOptionPane.INFORMATION_MESSAGE);
        String strB = JOptionPane.showInputDialog(null, "Vui lòng nhập b: ", "ax + b = c", JOptionPane.INFORMATION_MESSAGE);
        String strC = JOptionPane.showInputDialog(null, "Vui lòng nhập c: ", "ax + b = c", JOptionPane.INFORMATION_MESSAGE);
        String strD = JOptionPane.showInputDialog(null, "Vui lòng nhập d: ", "dy + e = f", JOptionPane.INFORMATION_MESSAGE);
        String strE = JOptionPane.showInputDialog(null, "Vui lòng nhập e: ", "dy + e = f", JOptionPane.INFORMATION_MESSAGE);
        String strF = JOptionPane.showInputDialog(null, "Vui lòng nhập f: ", "dy + e = f", JOptionPane.INFORMATION_MESSAGE);

        double a = Double.parseDouble(strA);
        double b = Double.parseDouble(strB);
        double c = Double.parseDouble(strC);
        double d = Double.parseDouble(strD);
        double e = Double.parseDouble(strE);
        double f = Double.parseDouble(strF);
        double res1 = 0;
        double res2 = 0;
        
        if (a == 0 || d == 0){
            JOptionPane.showMessageDialog(null, "Hệ phương trình vô nghiệm", "Result", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        } else{
            res1 = ((c-b)/a);
            res2 = ((f-e)/d);
        }

        JOptionPane.showMessageDialog(null, "Nghiệm của hệ phương trình (x,y) = (" + Double.toString(res1)+","+ Double.toString(res2)+")", "Result", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
