package homework;
import javax.swing.JOptionPane;

public class FirstDegreeEquation {
    public static void main(String[] args){
        JOptionPane.showMessageDialog(null, "Bạn đang ở chương trình giải phương trình bậc nhất ax + b = c\n", "Hello", JOptionPane.INFORMATION_MESSAGE);
        String strA = JOptionPane.showInputDialog(null, "Vui lòng nhập a: ");
        String strB = JOptionPane.showInputDialog(null, "Vui lòng nhập b: ");
        String strC = JOptionPane.showInputDialog(null, "Vui lòng nhập c: ");

        double a = Double.parseDouble(strA);
        double b = Double.parseDouble(strB);
        double c = Double.parseDouble(strC);
        double res = 0;
        
        if (a == 0){
            JOptionPane.showMessageDialog(null, "Phương trình vô nghiệm", "Result", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        } else{
            res = ((c-b)/a);
        }

        JOptionPane.showMessageDialog(null, "Nghiệm của phương trình là x = " + Double.toString(res), "Result", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
