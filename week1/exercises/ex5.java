package exercises;
import javax.swing.JOptionPane;
public class ex5 {
    public static void main(String[] args){
        String strFirst = JOptionPane.showInputDialog(null, "Input the first number: ", "Input", JOptionPane.INFORMATION_MESSAGE);
        String strSecond = JOptionPane.showInputDialog(null, "Input the second number: ", "Input", JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strFirst);
        double num2 = Double.parseDouble(strSecond);

        double total = num1 + num2;
        double diff = num1 - num2;
        double product = num1 * num2;
        String quotient = (num2 == 0) ? "None": Double.toString(num1 / num2);

        String strOut = "You input " + num1 + " and " + num2 + "\n" + "Total = " + total + "\nDifferrence = " + diff + "\nProduct = " + product + "\nQuotient = " + quotient + "\n";
        JOptionPane.showMessageDialog(null, strOut, "Awesome Tool", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}
