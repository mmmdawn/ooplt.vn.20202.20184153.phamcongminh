package exercises;
import javax.swing.JOptionPane;
public class TestDialog {
    public static void main(String[] args){
        String result;
        result = JOptionPane.showInputDialog("Nhập chiều cao của bạn (cm)");
        JOptionPane.showMessageDialog(null, "Chiều cao của bạn là " + result + " cm.");
        System.exit(0);
    }
}
