package hust.soict.hedspi.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {
    public static void main(String[] args) {
        try {
        File f = new File("./bigfile.txt");
        Scanner sc = new Scanner(f);
        StringBuilder s = new StringBuilder();
        while (sc.hasNextLine()) {
            s.append(sc.nextLine());
        }
        sc.close();
        System.out.println(s.toString());
    } catch (FileNotFoundException e) {
        System.out.print("File Not Found");
    }
}
}