package homework;

public class AddMatrix {

    static boolean isTheSameLevel( int[][] a, int[][] b ) {
        if ( a.length != b.length ) return false;
        for ( int i = 0; i < a.length; ++i ) {
            if ( a[i].length != b[i].length ) return false;
        }
        return true;
    }
    public static void main(String[] args) {
        int[][] a = { {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12} };
        int[][] b = { {6, 7, 4, 8}, {8, 12, 15, 5}, {6, 10, 11, 9} };

        if ( !isTheSameLevel(a, b) ) {
            System.out.println("Hai ma trận này không cùng cấp\n");
            System.exit(1);
        }

        int row = a.length;
        int column = a[0].length;
        int[][] res = new int[row][column];

        // Cộng hai ma trận
        for ( int i = 0; i < row; ++i ) {
            for ( int j = 0; j < column; ++j ) {
                res[i][j] = a[i][j] + b[i][j];
            }
        }
        
        // In ra

        //A
        System.out.println("Ma trận A: ");
        for ( int i = 0; i < row; ++i ) {
            for ( int j = 0; j < column; ++j ) {
                System.out.print(a[i][j] + "\t");
            }
            System.out.print("\n\n");
        }
        // B
        System.out.println("Ma trận B: ");
        for ( int i = 0; i < row; ++i ) {
            for ( int j = 0; j < column; ++j ) {
                System.out.print(b[i][j] + "\t");
            }
            System.out.print("\n\n");
        }
         // result
        System.out.println("Ma trận tổng của A và B: ");
        for ( int i = 0; i < row; ++i ) {
            for ( int j = 0; j < column; ++j ) {
                System.out.print(res[i][j] + "\t");
            }
            System.out.print("\n\n");
        }
    }
}