package homework;

import java.lang.reflect.Array;
import java.util.Arrays;

public class NumericArray {
    public static void main(String[] args) {
        int sum = 0;
        double average = 0;
        int[] array = {1,2,3,4,5,6};
        Arrays.sort(array);

        for (int i = 0 ; i < array.length; ++i ) {
            sum += array[i];
        }

        average =  ( (double) sum / array.length );

        for (int i = 0 ; i < array.length; ++i ) {
            System.out.print(array[i] + " ");
        }

        System.out.print("\n");
        System.out.println("Sum: " + sum);
        System.out.println("Average: " + average);


    }
}