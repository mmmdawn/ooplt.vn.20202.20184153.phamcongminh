package exercises;

import java.util.Scanner;

import java.util.Scanner;
public class DayOfMonth {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Input the year: ");
        int year = keyboard.nextInt();
        System.out.println("Input the month: ");
        int month = keyboard.nextInt();
        int day = 0;
        keyboard.close();

        if (month == 1) {
            day = 31;
        } else if ( month == 3 ) {
            day = 31;
        } else if ( month == 4 ) {
            day = 30;
        } else if ( month == 5 ) {
            day = 31;
        } else if ( month == 6 ) {
            day = 30;
        } else if ( month == 7 ) {
            day = 31;
        } else if ( month == 8 ) {
            day = 31;
        }else if ( month == 9 ) {
            day = 30;
        }else if ( month == 10 ) {
            day = 31;
        }else if ( month == 11 ) {
            day = 30;
        }else if ( month == 12 ) {
            day = 31;
        } else if ( month == 2 && (year%4==0) ) {
            day = 29;
        } else if ( month == 2 && (year%4 != 0) ) {
            day =28;
        }
        
        System.out.print("This month has " + day + " days.\n");

    }
}
