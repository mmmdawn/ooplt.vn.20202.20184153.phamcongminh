package exercises;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int n = keyboard.nextInt();
        int count = 0;
        for ( int i = 0; i < n; ++i ) {
            for ( int j = 0; j < n - i - 1; ++j ) {
                System.out.print(" ");
            }
            
            for ( int j = 0; j < i + 1 + count; ++j ) {
                System.out.print("*");
            }
            System.out.print("\n");

            count += 1;
        }

        keyboard.close();
    }
}
