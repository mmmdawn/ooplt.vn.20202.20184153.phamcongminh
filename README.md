# INTRODUCTION
***
Hi, I'm Minh, a student from Hanoi University of Science and Technology (HUST).  
My major at university is about computer, programming and Japanese, too !  
This semester, the subject that I look forward to most is Oriented Language and Theory (OOLT for short) and this repository contains my assignments of OOLT class.  
***
## *Thank you for your attention !*